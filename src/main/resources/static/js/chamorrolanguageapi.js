let api_url = "/data";

function executeSearch(text) {
    if (text === "") {
        resetTable();
        return;
    }
    let url = $("option:selected").val() + "&searchTerm=" + text;
    populateTable(url);
    updateSearchComponents(url, text)
}

function findWordsThatBeginWith(element) {
    let url = $(element).attr("data-url") + "&searchTerm=" + $(element).html();
    populateTable(url);
    updateSearchComponents(url, $(element).html());
    return false;
}

function updateSearchComponents(url, text) {
    $("#go-to-page-btn").attr("data-url", url);
    $("#go-to-page-input").val("");
    $("#search-term").html("<span class='search-term'>" + text + "</span>: ");
    $("#clear-btn").removeAttr("disabled");
}

function goToPage(pageNumber) {
    if (pageNumber === "") {
        alert("Please enter a number.");
        return;
    }
    if (!pageNumber.match(/^\d+$/)) {
        alert("Please enter only whole numbers (for example, 34).")
        return;
    }
    let totalPages = localStorage.getItem("total-number-of-pages");
    let number = parseInt(pageNumber);
    if (number < 1 || number > totalPages) {
        alert("Please enter a page number between 1 and " + totalPages + ".");
        $("#go-to-page-input").val("");
        return;
    }
    let gotopage_url = $("#go-to-page-btn").attr("data-url");
    let page = parseInt(pageNumber) - 1;
    if (gotopage_url) { // A search has been performed.
        populateTable(gotopage_url + "&page=" + page);
    } else {
        populateTable(api_url + "?page=" + page);
    }
    $("#go-to-page-input").val("");
}

function formatDate(datetime) {
    return new Date(datetime).toLocaleDateString('en-US', {
        month: 'long',
        day: 'numeric',
        year: 'numeric'
    });
}

function populateSearchDropdown() {
    let searchUrl = "/searchTypes";
    $.getJSON( searchUrl, function( data ) {
        let links = data._links;
        $.each(links, function(key, value) {
            let option = "<option value='" + value["href"] + "'>" + value["displayText"] + "</option>";
            $("select").append(option);
            if (value["href"].includes("StartsWith")) {
                $(".letter").attr("data-url", value["href"]);
            }
        });
        $("option").first().attr("selected", true);
    });
}

function formatNumberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function populateTable(url) {
    $.getJSON( url, function( data ) {
        $('.entry-row').remove();

        let entries = data._embedded.entries;

        let totalElements = data.page.totalElements;
        let totalEntriesSpan = formatNumberWithCommas(totalElements);
        let text = totalEntriesSpan + (totalElements === 1 ? " entry" : " entries");
        $("#total-entries").html(text);

        let totalPages = data.page.totalPages;
        if (totalPages > 1) {
            $("#go-to-page-btn").removeAttr("disabled");
        } else {
            $("#go-to-page-btn").attr("disabled", true);
        }
        localStorage.setItem("total-number-of-pages", totalPages);
        $(".page_number").html("Page " + (data.page.number + 1) + " of " + totalPages);

        // initialize the buttons
        let links = data._links;
        let first = links["first"];
        if (first) {
            $(".first_btn").attr("data-url", first["href"]);
        }
        if (first == null || data.page.number === 0) {
            $(".first_btn").attr("disabled", true);
        } else {
            $(".first_btn").removeAttr("disabled");
        }

        let prev = links["prev"];
        if (prev) {
            $(".prev_btn").attr("data-url", prev["href"]);
            $(".prev_btn").removeAttr("disabled");
        } else {
            $(".prev_btn").attr("disabled", true);
        }

        let next = links["next"];
        if (next) {
            $(".next_btn").attr("data-url", next["href"]);
            $(".next_btn").removeAttr("disabled");
        } else {
            $(".next_btn").attr("disabled", true);
        }

        let last = links["last"];
        if (last) {
            $(".last_btn").attr("data-url", last["href"]);
        }
        if (last == null || data.page.number === totalPages - 1) {
            $(".last_btn").attr("disabled", true);
        } else {
            $(".last_btn").removeAttr("disabled");
        }

        // iterate through each entry and populate table
        let tableRows = "";
        for (let i = 0; i < entries.length; i++)
        {
            let html = "<tr class='entry-row " + (i % 2 === 0 ? "even" : "odd") + "'>"
            let entry = entries[i];
            html += "<td style='width: 150px'>" + entry["entry"] + "</td>";
            html += "<td style='width: 150px'>" + (entry["pronunciation"] == null ? "" : entry["pronunciation"]) + "</td>";
            html += "<td>" + entry["meaning"] + "</td>";
            html += "<td style='width: 275px'>" + (entry["etymology"] == null ? "" : entry["etymology"]) + "</td>";
            html += "<td style='width: 275px'>" + (entry["examples"] == null ? "" : entry["examples"]) + "</td>";
            html += "<td style='width: 275px'>" + (entry["comments"] == null ? "" : entry["comments"]) + "</td>";
            html += "<td style='width: 175px'>" + formatDate(entry["createdOn"]) + "</td>";
            html += "<td style='width: 175px'>" + formatDate(entry["lastUpdatedOn"]) + "</td>";
            html += "</tr>";
            tableRows += html;
        }
        $("thead").append(tableRows);
    });
}

function resetTable() {
    populateTable(api_url);
    $("#clear-btn").attr("disabled", true);
    $("#go-to-page-btn").attr("data-url", "");
    $("#search-input").val("");
    $("#go-to-page-input").val("");
    $("#search-term").html("");
}

$(document).ready(function() {
    populateSearchDropdown();
    populateTable(api_url);
    $("#year").html(new Date().getFullYear());
    const menuBar = "A Å B CH D E F G H I K L M N Ñ NG O P R S T U Y -"
        .split(" ")
        .map(
            (letter) => `<a href="#" onclick="findWordsThatBeginWith(this)" class="letter" data-url="">${letter}</a> `
        ).join("");
    $("#nav-menu").append(menuBar);
});
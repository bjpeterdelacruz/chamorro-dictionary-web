/**
 * Jackson will convert JSON strings into instances of classes in this package.
 */
package dev.bjdelacruz.chamorrolanguage.json;

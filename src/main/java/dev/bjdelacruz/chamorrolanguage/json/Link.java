package dev.bjdelacruz.chamorrolanguage.json;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.Objects;

@SuppressFBWarnings("PI_DO_NOT_REUSE_PUBLIC_IDENTIFIERS_CLASS_NAMES")
public class Link {

    private String href;

    public Link() {
    }

    public Link(String href) {
        this.href = href;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public void replaceHref(String target, String replacement) {
        this.href = this.href.replace(target, replacement);
    }

    public Link replaceCharacters(String charsToReplace, String replaceWith) {
        this.href = this.href.replace(charsToReplace, replaceWith);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Link other && href.equals(other.href);
    }

    @Override
    public int hashCode() {
        return Objects.hash(href);
    }

    @Override
    public String toString() {
        return "Link{href='" + href + '}';
    }
}

package dev.bjdelacruz.chamorrolanguage.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchLink {

    private String href;
    private String displayText;

    public SearchLink() {
    }

    public SearchLink(String href, String displayText) {
        this.href = href;
        this.displayText = displayText;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof SearchLink other && href.equals(other.href) && displayText.equals(other.displayText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(href, displayText);
    }
}

package dev.bjdelacruz.chamorrolanguage.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@SuppressFBWarnings("EI_EXPOSE_REP")
public class EmbeddedLinks {

    @JsonProperty(value = "_embedded")
    private Map<String, List<DictionaryEntry>> embedded;

    @JsonProperty(value = "_links")
    private Map<String, Link> links;

    private Map<String, Integer> page;

    public Map<String, List<DictionaryEntry>> getEmbedded() {
        return embedded;
    }

    public void setEmbedded(Map<String, List<DictionaryEntry>> embedded) {
        this.embedded = embedded;
    }

    public Map<String, Link> getLinks() {
        return links;
    }

    public void setLinks(Map<String, Link> links) {
        this.links = links;
    }

    public Map<String, Integer> getPage() {
        return page;
    }

    public void setPage(Map<String, Integer> page) {
        this.page = page;
    }

    public void clearLinksInEmbedded() {
        embedded.values().forEach(entries -> entries.forEach(entry -> entry.setLinks(new HashMap<>())));
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof EmbeddedLinks other && embedded.equals(other.embedded) && links.equals(other.links)
                && page.equals(other.page);
    }

    @Override
    public int hashCode() {
        return Objects.hash(embedded, links, page);
    }

    @Override
    public String toString() {
        return "EmbeddedLinksPage{"
                + "embedded=" + embedded
                + ", links=" + links
                + ", page=" + page
                + '}';
    }
}

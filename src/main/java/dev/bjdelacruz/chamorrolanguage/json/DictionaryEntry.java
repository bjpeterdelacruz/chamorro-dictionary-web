package dev.bjdelacruz.chamorrolanguage.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class DictionaryEntry {

    @JsonProperty(value = "entry")
    private String word;
    private String pronunciation;
    private String meaning;
    private String etymology;
    private String examples;
    private String comments;
    private Date createdOn;
    private Date lastUpdatedOn;

    @JsonProperty(value = "_links")
    @SuppressFBWarnings("EI_EXPOSE_REP")
    private Map<String, Link> links;

    public DictionaryEntry() {
        var date = new Date();
        createdOn = date;
        lastUpdatedOn = date;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getPronunciation() {
        return pronunciation;
    }

    public void setPronunciation(String pronunciation) {
        this.pronunciation = pronunciation;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public String getEtymology() {
        return etymology;
    }

    public void setEtymology(String etymology) {
        this.etymology = etymology;
    }

    public String getExamples() {
        return examples;
    }

    public void setExamples(String examples) {
        this.examples = examples;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getCreatedOn() {
        return new Date(createdOn.getTime());
    }

    public void setCreatedOn(Date createdOn) {
        Objects.requireNonNull(createdOn);
        this.createdOn = new Date(createdOn.getTime());
    }

    public Date getLastUpdatedOn() {
        return new Date(lastUpdatedOn.getTime());
    }

    public void setLastUpdatedOn(Date lastUpdatedOn) {
        Objects.requireNonNull(lastUpdatedOn);
        this.lastUpdatedOn = new Date(lastUpdatedOn.getTime());
    }

    public Map<String, Link> getLinks() {
        return links;
    }

    public void setLinks(Map<String, Link> links) {
        this.links = links;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof DictionaryEntry other && word.equals(other.word)
                && Objects.equals(pronunciation, other.pronunciation)
                && meaning.equals(other.meaning) && Objects.equals(etymology, other.etymology)
                && Objects.equals(examples, other.examples) && Objects.equals(comments, other.comments)
                && createdOn.equals(other.createdOn) && lastUpdatedOn.equals(other.lastUpdatedOn)
                && links.equals(other.links);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word, pronunciation, meaning, etymology, examples, comments, createdOn, lastUpdatedOn, links);
    }

    @Override
    public String toString() {
        return "Entry{"
                + "entryName='" + word + '\''
                + ", pronunciation='" + pronunciation + '\''
                + ", meaning='" + meaning + '\''
                + ", etymology='" + etymology + '\''
                + ", examples='" + examples + '\''
                + ", comments='" + comments + '\''
                + ", createdOn=" + createdOn
                + ", lastUpdatedOn=" + lastUpdatedOn
                + ", links=" + links
                + '}';
    }
}

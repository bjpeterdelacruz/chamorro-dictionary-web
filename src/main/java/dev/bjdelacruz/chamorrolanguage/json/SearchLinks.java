package dev.bjdelacruz.chamorrolanguage.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SearchLinks {

    @JsonProperty(value = "_links")
    @SuppressFBWarnings("EI_EXPOSE_REP")
    private Map<String, SearchLink> links = new HashMap<>();

    public Map<String, SearchLink> getLinks() {
        return links;
    }

    public void setLinks(Map<String, SearchLink> links) {
        this.links = links;
    }

    public void removeLink(String link) {
        this.links.remove(link);
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof SearchLinks other && links.equals(other.links);
    }

    @Override
    public int hashCode() {
        return Objects.hash(links);
    }
}

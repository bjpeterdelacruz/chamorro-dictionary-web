package dev.bjdelacruz.chamorrolanguage.controllers;

import dev.bjdelacruz.chamorrolanguage.json.SearchLinks;
import dev.bjdelacruz.chamorrolanguage.utils.JSONUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class WebController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebController.class);

    @Value("${api.url.entries}")
    private String httpsEntriesEndpoint;

    @Value("${api.url.search}")
    private String httpsSearchEndpoint;

    @Value("${webapp.base-path}")
    private String webappBasePath;

    @GetMapping("/searchTypes")
    public SearchLinks getAllSearchTypes() {
        var body = getData(httpsSearchEndpoint, null, null);
        try {
            return JSONUtils.updateSearchLinks(body);
        }
        catch (JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GetMapping("/search")
    public Object search(@RequestParam(required = false) String searchBy, @RequestParam String searchTerm,
                         @RequestParam(required = false) String page, @RequestParam(required = false) String size) {
        var url = httpsSearchEndpoint + "/" + searchBy;
        if (searchBy.endsWith("Contains")) {
            url += "?contains=" + searchTerm;
        }
        else if (searchBy.endsWith("Equals")) {
            url += "?equals=" + searchTerm;
        }
        else if (searchBy.endsWith("StartsWith")) {
            url += "?startsWith=" + searchTerm;
        }
        else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Invalid search type: %s", searchBy));
        }

        var body = getData(url, page, size);
        var searchUrl = String.format("/search?searchBy=%s&searchTerm=%s", searchBy, searchTerm);
        try {
            return JSONUtils.updateLinks(body, url.replace("https", "http"), searchUrl);
        }
        catch (JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GetMapping(value = "/data", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object data(@RequestParam(required = false) String page, @RequestParam(required = false) String size) {
        var body = getData(httpsEntriesEndpoint, page, size);
        try {
            return JSONUtils.updateLinks(body, httpsEntriesEndpoint, webappBasePath);
        }
        catch (JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    private static String addPageSizeParams(String page, String size) {
        if (page != null && size != null) {
            return String.format("page=%s&size=%s", page, size);
        }
        else if (page != null) {
            return String.format("page=%s", page);
        }
        else if (size != null) {
            return String.format("size=%s", size);
        }
        return "";
    }

    private static String getData(String urlEndpoint, String page, String size) {
        var restTemplate = new RestTemplate();

        var url = urlEndpoint + (urlEndpoint.contains("?") ? '&' : '?') + addPageSizeParams(page, size);
        LOGGER.info("GET: {}", url);

        var headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        return restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class).getBody();
    }
}

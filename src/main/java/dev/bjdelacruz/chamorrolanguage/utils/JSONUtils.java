package dev.bjdelacruz.chamorrolanguage.utils;

import dev.bjdelacruz.chamorrolanguage.json.EmbeddedLinks;
import dev.bjdelacruz.chamorrolanguage.json.SearchLink;
import dev.bjdelacruz.chamorrolanguage.json.SearchLinks;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class JSONUtils {

    private JSONUtils() {
    }

    /**
     * Updates the search URLs in the given JSON string.
     *
     * @param jsonString The JSON string that contains URLs.
     * @throws JsonProcessingException if there are problems reading in the JSON string.
     * @return An object that contains updated search URLs.
     */
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public static SearchLinks updateSearchLinks(String jsonString) throws JsonProcessingException {
        var mapper = new ObjectMapper();
        var links = mapper.readValue(jsonString, SearchLinks.class);
        links.removeLink("self");
        var searchLinks = new SearchLinks();
        for (var link : links.getLinks().values()) {
            var startIdx = link.getHref().lastIndexOf('/') + 1;
            var endIdx = link.getHref().indexOf('{');
            var displayName = link.getHref().substring(startIdx, endIdx)
                    // e.g., "findByEntryStartsWith" becomes "Entry Starts With"
                    .replaceAll("findBy", "").replaceAll("(.)([A-Z])", "$1 $2");
            var searchLink = new SearchLink(
                    String.format("/search?searchBy=%s", link.getHref().substring(startIdx, endIdx)), displayName);
            searchLinks.getLinks().put(link.getHref().substring(startIdx, endIdx), searchLink);
        }
        return searchLinks;
    }

    /**
     * Updates URLs in the given JSON string.
     *
     * @param jsonString The JSON string that contains URLs.
     * @param urlToReplace The URL to replace in the given JSON string.
     * @param replaceWith The string to replace the URL with.
     * @throws JsonProcessingException if there are problems reading in the JSON string.
     * @return An object that contains updated URLs.
     */
    public static EmbeddedLinks updateLinks(String jsonString, String urlToReplace, String replaceWith) throws JsonProcessingException {
        var mapper = new ObjectMapper();
        var embeddedLinks = mapper.readValue(jsonString, EmbeddedLinks.class);
        embeddedLinks.clearLinksInEmbedded();
        var links = embeddedLinks.getLinks();
        links.remove("profile");
        links.remove("search");
        for (var link : links.values()) {
            link.replaceCharacters("%C3%85", "Å")
                    .replaceCharacters("%C3%A5", "å")
                    .replaceCharacters("%C3%91", "Ñ")
                    .replaceCharacters("%C3%B1", "ñ");
            link.replaceHref(urlToReplace, replaceWith);
        }
        return embeddedLinks;
    }
}

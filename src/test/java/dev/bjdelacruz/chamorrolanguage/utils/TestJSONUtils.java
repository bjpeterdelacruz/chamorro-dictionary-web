package dev.bjdelacruz.chamorrolanguage.utils;

import dev.bjdelacruz.chamorrolanguage.json.EmbeddedLinks;
import dev.bjdelacruz.chamorrolanguage.json.DictionaryEntry;
import dev.bjdelacruz.chamorrolanguage.json.Link;
import dev.bjdelacruz.chamorrolanguage.json.SearchLink;
import dev.bjdelacruz.chamorrolanguage.json.SearchLinks;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class TestJSONUtils {

    @Test
    public void testUpdateLinks1() throws Exception {
        var expected = new EmbeddedLinks();
        var entry = new DictionaryEntry();
        entry.setWord("a-");
        entry.setPronunciation("æ");
        entry.setMeaning("Reciprocal marker, do (something) to each other - prefix. May be affixed to verbs. (when attached it forces the stress to fall on the a-)");
        var format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.US);
        entry.setCreatedOn(format.parse("2020-12-14T20:18:17.342+00:00"));
        entry.setLastUpdatedOn(format.parse("2020-12-14T20:18:17.471+00:00"));
        entry.setLinks(new HashMap<>());

        var embedded = new HashMap<String, List<DictionaryEntry>>();
        embedded.put("entries", List.of(entry));
        expected.setEmbedded(embedded);

        var links = new LinkedHashMap<String, Link>();
        links.put("first", new Link("/data?page=0&size=1"));
        links.put("self", new Link("/data?page=0&size=1"));
        links.put("next", new Link("/data?page=1&size=1"));
        links.put("last", new Link("/data?page=10209&size=1"));
        expected.setLinks(links);

        var page = new HashMap<String, Integer>();
        page.put("size", 1);
        page.put("totalElements", 10210);
        page.put("totalPages", 10210);
        page.put("number", 0);
        expected.setPage(page);

        var jsonString = """
                {
                   "_embedded":{
                      "entries":[
                         {
                            "entry":"a-",
                            "pronunciation":"æ",
                            "meaning":"Reciprocal marker, do (something) to each other - prefix. May be affixed to verbs. (when attached it forces the stress to fall on the a-)",
                            "etymology":null,
                            "examples":null,
                            "comments":null,
                            "createdOn":"2020-12-14T20:18:17.342+00:00",
                            "lastUpdatedOn":"2020-12-14T20:18:17.471+00:00",
                            "_links":{
                               "self":{
                                  "href":"http://localhost:9000/api/entries/1"
                               },
                               "entry":{
                                  "href":"http://localhost:9000/api/entries/1"
                               }
                            }
                         }
                      ]
                   },
                   "_links":{
                      "first":{
                         "href":"http://localhost:9000/api/entries?page=0&size=1"
                      },
                      "self":{
                         "href":"http://localhost:9000/api/entries?page=0&size=1"
                      },
                      "next":{
                         "href":"http://localhost:9000/api/entries?page=1&size=1"
                      },
                      "last":{
                         "href":"http://localhost:9000/api/entries?page=10209&size=1"
                      },
                      "profile":{
                         "href":"http://localhost:9000/api/profile/entries"
                      },
                      "search":{
                         "href":"http://localhost:9000/api/entries/search"
                      }
                   },
                   "page":{
                      "size":1,
                      "totalElements":10210,
                      "totalPages":10210,
                      "number":0
                   }
                }""";
        var actual = JSONUtils.updateLinks(jsonString, "http://localhost:9000/api/entries", "/data");
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testUpdateLinks2() throws Exception {
        var expected = new EmbeddedLinks();
        var entry = new DictionaryEntry();
        entry.setWord("å'å'");
        entry.setMeaning("verb. to open one's mouth wide; to gape.");
        entry.setComments("ångla'a");
        var format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.US);
        entry.setCreatedOn(format.parse("2020-12-14T20:18:17.342+00:00"));
        entry.setLastUpdatedOn(format.parse("2020-12-14T20:18:17.471+00:00"));
        entry.setLinks(new HashMap<>());

        var embedded = new HashMap<String, List<DictionaryEntry>>();
        embedded.put("entries", List.of(entry));
        expected.setEmbedded(embedded);

        var links = new LinkedHashMap<String, Link>();
        links.put("first", new Link("/search?searchBy=findByEntryStartsWith&searchTerm=Å&page=0&size=1"));
        links.put("self", new Link("/search?searchBy=findByEntryStartsWith&searchTerm=Å&page=0&size=1"));
        links.put("next", new Link("/search?searchBy=findByEntryStartsWith&searchTerm=Å&page=1&size=1"));
        links.put("last", new Link("/search?searchBy=findByEntryStartsWith&searchTerm=Å&page=851&size=1"));
        expected.setLinks(links);

        var page = new HashMap<String, Integer>();
        page.put("size", 1);
        page.put("totalElements", 852);
        page.put("totalPages", 852);
        page.put("number", 0);
        expected.setPage(page);

        var jsonString = """
                {
                   "_embedded":{
                      "entries":[
                         {
                            "entry":"å'å'",
                            "pronunciation":null,
                            "meaning":"verb. to open one's mouth wide; to gape.",
                            "etymology":null,
                            "examples":null,
                            "comments":"ångla'a",
                            "createdOn":"2020-12-14T20:18:17.342+00:00",
                            "lastUpdatedOn":"2020-12-14T20:18:17.471+00:00",
                            "_links":{
                               "self":{
                                  "href":"http://localhost:9000/api/entries/247"
                               },
                               "entry":{
                                  "href":"http://localhost:9000/api/entries/247"
                               }
                            }
                         }
                      ]
                   },
                   "_links":{
                      "first":{
                         "href":"http://localhost:9000/api/entries/search/findByEntryStartsWith?startsWith=%C3%85&page=0&size=1"
                      },
                      "self":{
                         "href":"http://localhost:9000/api/entries/search/findByEntryStartsWith?startsWith=%C3%85&page=0&size=1"
                      },
                      "next":{
                         "href":"http://localhost:9000/api/entries/search/findByEntryStartsWith?startsWith=%C3%85&page=1&size=1"
                      },
                      "last":{
                         "href":"http://localhost:9000/api/entries/search/findByEntryStartsWith?startsWith=%C3%85&page=851&size=1"
                      }
                   },
                   "page":{
                      "size":1,
                      "totalElements":852,
                      "totalPages":852,
                      "number":0
                   }
                }""";
        var actual = JSONUtils.updateLinks(jsonString,
                "http://localhost:9000/api/entries/search/findByEntryStartsWith?startsWith=Å",
                "/search?searchBy=findByEntryStartsWith&searchTerm=Å");
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testUpdateSearchLinks() throws JsonProcessingException {
        var jsonString = """
                {
                   "_links":{
                      "findByMeaningContains":{
                         "href":"http://localhost:9000/api/entries/search/findByMeaningContains{?contains,page,size,sort}",
                         "templated":true
                      },
                      "findByExamplesContains":{
                         "href":"http://localhost:9000/api/entries/search/findByExamplesContains{?contains,page,size,sort}",
                         "templated":true
                      },
                      "findByEntryContains":{
                         "href":"http://localhost:9000/api/entries/search/findByEntryContains{?contains,page,size,sort}",
                         "templated":true
                      },
                      "findByEntryEquals":{
                         "href":"http://localhost:9000/api/entries/search/findByEntryEquals{?equals,page,size,sort}",
                         "templated":true
                      },
                      "findByCommentsContains":{
                         "href":"http://localhost:9000/api/entries/search/findByCommentsContains{?contains,page,size,sort}",
                         "templated":true
                      },
                      "findByEntryStartsWith":{
                         "href":"http://localhost:9000/api/entries/search/findByEntryStartsWith{?startsWith,page,size,sort}",
                         "templated":true
                      },
                      "self":{
                         "href":"http://localhost:9000/api/entries/search/"
                      }
                   }
                }""";
        var expected = new SearchLinks();
        var links = Map.of(
                "findByMeaningContains", new SearchLink("/search?searchBy=findByMeaningContains", "Meaning Contains"),
                "findByExamplesContains", new SearchLink("/search?searchBy=findByExamplesContains", "Examples Contains"),
                "findByEntryContains", new SearchLink("/search?searchBy=findByEntryContains", "Entry Contains"),
                "findByEntryEquals", new SearchLink("/search?searchBy=findByEntryEquals", "Entry Equals"),
                "findByCommentsContains", new SearchLink("/search?searchBy=findByCommentsContains", "Comments Contains"),
                "findByEntryStartsWith", new SearchLink("/search?searchBy=findByEntryStartsWith", "Entry Starts With"));
        expected.setLinks(links);

        var actual = JSONUtils.updateSearchLinks(jsonString);
        Assertions.assertEquals(expected, actual);
    }
}

package dev.bjdelacruz.chamorrolanguage.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.EnabledIf;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WebController.class)
@EnabledIf(expression = "#{environment['spring.profiles.active'] == 'integration'}", loadContext = true)
public class TestWebController {

    @Autowired
    private MockMvc mvc;

    @Test
    public void data() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/data")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void dataPage() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/data?page=0")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void datePageSize() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/data?page=0&size=20")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void dataSize() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/data?size=1")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void search() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/search")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void searchContains() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/search?searchBy=findByEntryEquals&searchTerm=biba")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void searchEquals() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/search?searchBy=findByEntryContains&searchTerm=Biba")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void searchStartsWith() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/search?searchBy=findByEntryStartsWith&searchTerm=CH")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void searchStartsWithPage() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/search?searchBy=findByEntryStartsWith&searchTerm=CH&page=2")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void searchStartsWithSize() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/search?searchBy=findByEntryStartsWith&searchTerm=CH&size=1")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void searchStartsWithPageSize() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/search?searchBy=findByEntryStartsWith&searchTerm=A&page=10&size=1")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void searchMissingSearchTerm() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/search?searchBy=findByEntryEquals")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("Missing searchTerm parameter"));
    }

    @Test
    public void searchInvalidSearchBy() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/search?searchBy=unknown&searchTerm=biba")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("Invalid search type: unknown"));
    }
}

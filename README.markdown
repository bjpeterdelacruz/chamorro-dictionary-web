# Chamorro Dictionary

This web application retrieves Chamorro dictionary entries from a remote API server. The web application and API are
both developed using Java and the Spring Framework, and they are running on Docker containers. The web application is
hosted on Google Cloud Platform, and the API is hosted on Microsoft Azure. The dictionary entries are stored in an
Azure SQL database.

## Live Demo

### Web Application

[https://chamorro-dictionary-web-knartlettq-uw.a.run.app](https://chamorro-dictionary-web-knartlettq-uw.a.run.app)

### API

[https://chamorro-language-api-web-app.azurewebsites.net/api/entries](https://chamorro-language-api-web-app.azurewebsites.net/api/entries)

## Developer Guide

### Pre-requisites

* Java Development Kit, version 23 or newer
* Docker Desktop, version 4.26.1 or newer
* [Chamorro Language API](https://bitbucket.org/bjpeterdelacruz/chamorro-language-api)

### Build Steps

1. Set the URL of the API in the `CHAMORRO_LANGUAGE_API_URL` environment variable.

2. Set the URL of the API to use for testing in the `CHAMORRO_LANGUAGE_API_URL_TEST` environment variable.

3. To enable integration tests, set the `spring.profiles.active` property in `src/test/resources/application.properties`
   to `integration`. The API must be reachable for the integration tests to pass.

4. Build the project:

```
gradle clean build
```

### Manually Deploying to Cloud Run on Google Cloud Platform

1. For **Container image URL**, enter `bjdelacruz/chamorro-dictionary-web`.
2. Select **Allow unauthenticated invocations**.
3. Create an environment variable called `CHAMORRO_LANGUAGE_API_URL` and enter the URL of the API to which this
   application makes a call.

### Deploying to Cloud Run on Google Cloud Platform via Terraform

The `gcp.tf` script inside the `terraform` directory contains two resources that will be deployed to Cloud Run. Simply
run the `terraform apply` command to deploy the resources to GCP. The value of `CHAMORRO_LANGUAGE_API_URL` will need to
be passed to the script.

### Building a Docker Image

1. In the Makefile, update `CHAMORRO_LANGUAGE_API_URL`.

2. Use the `make` command to build a Docker image and create a running container:

```
make
```

### Downloading the Latest Docker Image

```
docker pull bjdelacruz/chamorro-language-web:latest
```

## Screenshots

![Main Page](https://bjdelacruz.dev/files/chamorrodictionary.png "The main page")

![Letter](https://bjdelacruz.dev/files/chamorrodictionary-letter.png "Browsing through words that begin with Ñ")

![Search Results](https://bjdelacruz.dev/files/chamorrodictionary-search-results.png "Searching for a Chamorro word")

## See Also

[Chamorro Language Reference](https://bitbucket.org/bjpeterdelacruz/chamorro-language-reference) - a desktop version of
this web application that can run on macOS and Windows computers

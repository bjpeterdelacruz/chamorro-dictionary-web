terraform {
  backend "s3" {
    bucket = "dev-bjdelacruz-terraform"
    key    = "chamorro-dictionary-web/state.tfstate"
    region = "us-east-2"
  }
}

provider "google" {
  project = var.GOOGLE_CLOUD_PROJECT_ID
  region  = "us-west1"
}

variable "GOOGLE_CLOUD_PROJECT_ID" {
  type = string
}

variable "CHAMORRO_LANGUAGE_API_URL" {
  type = string
}

resource "google_cloud_run_service" "chamorro_dictionary_web" {
  name     = "chamorro-dictionary-web"
  location = "us-west1"

  template {
    spec {
      containers {
        image = "bjdelacruz/chamorro-dictionary-web:latest"

        resources {
          limits = {
            memory = "256Mi"
            cpu    = "1"
          }
        }

        env {
          name  = "CHAMORRO_LANGUAGE_API_URL"
          value = var.CHAMORRO_LANGUAGE_API_URL
        }
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service_iam_policy" "public_access" {
  location = google_cloud_run_service.chamorro_dictionary_web.location
  service  = google_cloud_run_service.chamorro_dictionary_web.name

  policy_data = <<EOF
{
  "bindings": [
    {
      "role": "roles/run.invoker",
      "members": [
        "allUsers"
      ]
    }
  ]
}
EOF
}

output "cloud_run_service_url" {
  description = "The URL of the deployed Cloud Run service"
  value       = google_cloud_run_service.chamorro_dictionary_web.status[0].url
}

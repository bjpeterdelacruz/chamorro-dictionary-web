FROM openjdk:23-oracle
WORKDIR /app
COPY build/libs/*.jar /app/chamorrodictionary.jar
CMD ["sh", "-c", "java -jar chamorrodictionary.jar"]
